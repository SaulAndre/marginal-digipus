from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import TemplateView

class IndexView(TemplateView):
    def get(self, request, *args, **kwargs):
        return JsonResponse({
            "status": "Success",
            "message": "Hello World"
        })
# Create your views here.
