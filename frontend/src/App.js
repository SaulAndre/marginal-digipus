// App.js
import React, { Component } from 'react';

class App extends Component {
  state = {
    data: {}
  };
  async componentDidMount() {
    try {
      const res = await fetch('http://127.0.0.1:8000/'); // fetching the data from api, before the page loaded
      const data = await res.json();
      this.setState({
        data
      });
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div>
        <h1>
          {this.state.data.status}
        </h1>
        <h2>
          {this.state.data.message}
        </h2>
      </div>
    );
  }
}

export default App;